import java.io.Serializable;



public class Activity implements Serializable {
String pacientID="123abc";
String StartDate;
String EndDate;
String Activity;

Activity(String StartDate, String EndDate, String Activity){
   this.StartDate=StartDate;
   this.EndDate=EndDate;
   this.Activity=Activity;
}
String GetStartDate(){return  StartDate;}
String GetEndDate(){
    return  EndDate;
}
String GetActivity(){
    return  Activity;
}

public String toString(){
    return "patient_id " +pacientID+"activity "+Activity+ "start "+StartDate +
            "end "+EndDate;

}
}
