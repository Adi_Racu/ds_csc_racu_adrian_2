
import com.rabbitmq.client.*;
import com.rabbitmq.client.Channel;

public class Consumer extends DefaultConsumer {
    public Consumer(Channel currentChanel){
        super(currentChanel);
    }
    public void handleDelivery(
            String consumerTag,
            Envelope envelope,
            BasicProperties properties,
            byte[] body)
            throws java.io.IOException {
        String message = new String(body);
        System.out.println("Received: " + message);
    }
}
